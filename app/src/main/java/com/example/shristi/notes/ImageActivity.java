package com.example.shristi.notes;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

public class ImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_image);


        ImageView imageView = (ImageView) findViewById(R.id.myImage);

        final Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    String state = Environment.getExternalStorageState();
                    if (Environment.MEDIA_MOUNTED.equals(state)) {
                        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "MobileSystemE");
                        Log.i("dirName", directory.getName());


                        if (!directory.exists()) {
                            directory.mkdir();
                            Log.i("make directory", "mkdir");
                        }
                        File  imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
                                .getAbsolutePath() + "/MobileSystemE/" + "note.png");
                        FileOutputStream out = new FileOutputStream(imageFile);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                        out.flush();
                        out.close();
                        MediaScannerConnection.scanFile(ImageActivity.this, new String[] {imageFile.toString()}, null, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

}

